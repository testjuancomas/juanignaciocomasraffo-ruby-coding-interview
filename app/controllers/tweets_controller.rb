class TweetsController < ApplicationController
  def index
    render json: Tweet.where("id > ?", params[:id]).where("id < ?",params[:id].to_i + 20).order("id desc")
  end
end
